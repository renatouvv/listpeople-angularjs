# listpeople-angularjs

This is my first angularjs app.

## Prerequisites

You must have node.js and its package manager (npm) installed.
You can get them from [http://nodejs.org/](http://nodejs.org/).

### First things first

I've created this first app using `npm` and `bower`
After the installation of npm I had to install bower, a node package, using the command below:

```
npm install bower
```

### Installing Dependencies

* I get the tools via `npm`, the [node package manager][npm].
* And get the angular code via `bower`, a [client-side code package manager][bower].

So, with those two, i just have to call:

```
npm install
```

This command will also call `bower install`. When the command finishes,  the two new
folders below will appear in your project.

* `node_modules` - contains the npm packages
* `app/bower_components` - contains the angular framework files

### Running the Application

The way to start
this server is using the command below, on the project folder:

```
npm start
```

Now you just have to browse to the app at `http://localhost:8000/app`.